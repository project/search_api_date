<?php

/**
 * @file
 * Search API Date.
 */

class SearchApiDateProcessor extends SearchApiAbstractProcessor {
  /**
   * Field settings.
   * @var array
   */
  protected $fieldSettings = array();

  /**
   * Store the settings for processing field.
   * @var array
   */
  protected $currentDateAttributes = array();

  /**
   * Map between attribute and date part.
   * @var array
   */
  protected $attributesMapping = array(
    'year'   => 'Y',
    'month'  => 'n',
    'day'    => 'j',
    'hour'   => 'G',
    'minute' => 'i',
    'second' => 's',
  );

  protected function getFieldSettings($field_id) {
    if (isset($this->fieldSettings[$field_id])) {
      return $this->fieldSettings[$field_id];
    }

    $field_settings = empty($this->options['fields'][$field_id]) ? array() : (is_array($this->options['fields'][$field_id]) ? $this->options['fields'][$field_id] : array());
    $field_settings += array('status' => 0, 'attributes' => array());
    $field_settings['attributes'] = array_combine($field_settings['attributes'], array_fill(0, count($field_settings['attributes']), 1));

    $field_settings['attributes'] += array(
      'year'   => 0,
      'month'  => 0,
      'day'    => 0,
      'hour'   => 0,
      'minute' => 0,
      'second' => 0,
    );

    $this->fieldSettings[$field_id] = $field_settings;
    return $field_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsIndex(SearchApiIndex $index) {
    foreach ($index->getFields() as $field) {
      if (isset($field['type']) && $field['type'] == 'date') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm() {
    $form = array();

    $form['fields'] = array(
      '#title' => t('Which fields should be altered?'),
      '#type'  => 'item',
      '#tree'  => TRUE,
      '#theme' => 'search_api_date_fields',
      '#empty' => '<p>' . t("Items indexed by this index aren't date types and therefore cannot be altered here.") . '</p>',
    );

    foreach ($this->index->getFields() as $field_id => $field) {
      if (isset($field['type']) && $field['type'] == 'date') {
        $settings = $this->getFieldSettings($field_id);

        $element = array(
          '#type' => 'item',
          '#tree' => TRUE,
          '#storage' => array(
            'field' => $field,
          ),
        );

        $element['status'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Status'),
          '#title_display' => 'invisible',
          '#return_value'  => '1',
          '#default_value' => $settings['status'],
        );

        $element['field'] = array(
          '#type'          => 'item',
          '#title'         => t('Field'),
          '#title_display' => 'invisible',
          '#markup'        => $field['name'],
        );

        $element['attributes'] = array(
          '#type'          => 'checkboxes',
          '#title'         => t('Date attributes to collect'),
          '#title_display' => 'invisible',
          '#options' => array(
            'year'   => t('Year'),
            'month'  => t('Month'),
            'day'    => t('Day'),
            'hour'   => t('Hour'),
            'minute' => t('Minute'),
            'second' => t('Second'),
          ),
          '#default_value' => array_keys(array_filter($settings['attributes'])),
        );

        $form['fields'][$field_id] = $element;
      }
    }

    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    $fields = empty($values['fields']) ? array() : $values['fields'];

    foreach ($fields as $field => $settings) {
      $attributes = array_filter($settings['attributes']);

      if (!empty($settings['status']) && empty($attributes)) {
        $field_info = $form['fields'][$field]['#storage']['field'];
        form_error($form['fields'][$field], t('You have to select at least one date attribute to collect for %field field.', array('%field' => $field_info['name'])));
        continue;
      }

      $values['fields'][$field]['attributes'] = array_keys($attributes);
    }
  }

  public function configurationFormSubmit(array $form, array &$values, array &$form_state) {
    $this->options = $values;
    return $values;
  }

  /**
   * Calls processField() for all appropriate fields.
   */
  public function preprocessIndexItems(array &$items) {
    foreach ($items as &$item) {
      foreach ($item as $name => &$field) {
        if ($this->testField($name, $field)) {
          $settings = $this->getFieldSettings($name);
          $this->currentDateAttributes = $settings['attributes'];
          $this->processField($field['value'], $field['type']);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function testField($name, array $field) {
    if ($field['type'] != 'date') {
      return FALSE;
    }

    return !empty($this->options['fields'][$name]['status']);
  }

  /**
   * {@inheritdoc}
   */
  protected function mktime($year, $month, $day, $hour, $minute, $second) {
    if (!$year) {
      $year = 1970;
    }

    if (!$month) {
      $month = 1;
    }

    if (!$day) {
      $day = 1;
    }

    return gmmktime($hour, $minute, $second, $month, $day, $year);
  }

  /**
   * {@inheritdoc}
   */
  protected function processTimestamp($timestamp, $attributes) {
    $parts = array();

    foreach($attributes as $attribute => $indexable) {
      $parts[$attribute] = $indexable ? trim(date($this->attributesMapping[$attribute], $timestamp), '0') : 0;
    }

    return $this->mktime($parts['year'], $parts['month'], $parts['day'], $parts['hour'], $parts['minute'], $parts['second']);
  }

  /**
   * {@inheritdoc}
   */
  protected function process(&$value) {
    if (is_numeric($value)) {
      $value = $this->processTimestamp($value, $this->currentDateAttributes);
    }
  }
}
